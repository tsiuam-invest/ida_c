/* 
 * Algoritmo IDA codificado por Dr. Marcelín Jiménez
 * Modificado para lenguaje C por Raúl Antonio Ortega Vallejo
 * UAMI, UAMC
 */

#include <stdio.h>
#include "ida.h"
/*  
 *  Test de IDA, Dispersa, Recupera 
 */

int main(int argc, char *argv[])
{
   puts("Testing IDA");
   if(argc < 3){ 
       printf("\nRequiere args:\n\t%s ruta_archivo dir_dispersales\n",argv[0]);
       return -1;
   }
   char archivo[256]= "\0";
   snprintf(archivo,256,"%s%s","recupera_",argv[1]);
   int d = ida_dispersa(argv[1],argv[2],".data");
   int r = ida_recupera(archivo,argv[2],".data");
   printf("\a\nInfo: ");
   printf("%d bytes dispersados ",d); 
   printf("en 5 archivos de %d bytes.",ida_tam(d));
   printf("\n\t%d bytes recuperados -> %s.\n",r,archivo);
   return 0;
}

