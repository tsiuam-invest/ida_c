#ifndef IDA_H
#define IDA_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Crea 5 archvos dispersales con ext. apartir de un dir_archivo
 *      Eje: ida_dispersa("movie.mp4","tmp/",".data").	
 *
 * Devuelve tamaño de dir_archivo, -1 si hay error.
*/
int ida_dispersa(char *dir_archivo,char *dir_disp,char *ext);
/**
 * Recupera un dir_archivo a partir de 3 dispersales en dir_disp
 *    Eje: ida_recupera("recoverMovie.mp4","tmp/",".data").	
 *
 * Devuelve tamaño de dir_archivo, -1 si hay error.
*/
int ida_recupera(char *dir_archivo,char *dir_disp,char *ext);
/**
 * Calcula el tamaño de un dispersal apartir de size.
 */
int ida_tam(int size);

/**
 * Habilita(1)/Deshabilita(0)
 *    mensajes del programa, excepto errores.
 */
#ifndef IDA_VERBOSE
#define IDA_VERBOSE (1)
#endif


/**
 * MODIFICAR CON PRECAUCION
 */
#ifndef ida_printf
/* Modificar la invocación printf por manejador logs */
#if IDA_VERBOSE 
    #define ida_printf printf 
#else
    #define ida_no_printf(...) NULL
    #define ida_printf ida_no_printf 
#endif
#endif 
/**
 * Cualquier error en ida_dispersa o ida_recupera 
 * no abortará la ejecución del programa invocador.
 */
#ifndef ida_return_if
#define ida_return_if(COND,INFO) if(COND) { perror(INFO); return -1; }
#endif


#endif

/* EOF */

