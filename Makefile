CC=gcc
IDA=ida
SRC=src/
MAIN=test
LIB=include
DIR_TEST=tmp_test/
FLAGS=-Wall -Wextra

.PHONY : clean test dir main

main :   
	$(CC) -o $(MAIN) $(MAIN).c  $(SRC)$(IDA).c -I$(LIB) $(FLAGS)

dir :  
	mkdir -p $(DIR_TEST)
exec: 
	./$(MAIN) $(MAIN).c $(DIR_TEST)  

test:   dir main exec

clean :
	rm -rf $(MAIN) $(DIR_TEST) *.o 

