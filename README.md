# ida_c
ida_c es una implementación de *Information dispersal algorithm* (IDA).  
## Uso
Incluir header  **ida.h**.  
El módulo proporciona las siguientes funciones tipo `int`:  
- **ida_dispersa** `(char*,char*,char*);`   
Dispersa un archivo del sistema en 5 archivos dispersales.  
Devuelve el tamaño del archivo leído, -1 si hubó error. 
- **ida_recupera** `(char*,char*,char*);`   
Recupera un archivo apartir de 3 archivos dispersales.    
Devuelve el tamaño del archivo recuperado, -1 si hubó error.  
- **ida_tam** `(int);`  
Devuelve el tamaño de un archivo dispersal. 
### Ejemplos
    ida_dispersa("movie.mp4", "/tmp/", ".data");
    ida_recupera("recover.mp4", "/tmp/", ".data");
    ida_tam(n_bytes("movie.mp4")); 
    ida_tam(ida_dispersa("movie.mp4", "/tmp/", ".data")); 
   El directorio de ejemplo *tmp* está almacenando los archivos dispersales. 
   Los cuales, tienen una extensión `.data`.   

> **Importante** 
> Epecificar el caracter '`/`' y acorde al sistema operativo. 

## Mensajes y logs 
[ida.h](include/ida.h)  dispone de macros como `IDA_VERBOSE` para omitir la impresión (habilitada por default). 


## Compilación
```bash
make 
```
### Demostración rápida 
```bash
make test
```
## Autores
- Marcelín Jiménez Ricardo. uami.  
- Lopéz Fuentes Francisco de Asís. uamc. 
- Ortega Vallejo Raúl Antonio. uamc 


